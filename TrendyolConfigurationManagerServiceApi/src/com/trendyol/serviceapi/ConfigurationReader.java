package com.trendyol.serviceapi;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.trendyol.serviceapi.entity.Property;


public class ConfigurationReader implements Runnable {
	
	private String propertyValue;
	
	private String applicationName;
	
	private String connectionString;
	
	private String propertyName;
	
	private long refreshTimerIntervalInMs;
	
	public ConfigurationReader(String applicationName, String connectionString, long refreshTimerIntervalInMs) {
		super();
		this.applicationName = applicationName;
		this.connectionString = connectionString;
		this.refreshTimerIntervalInMs = refreshTimerIntervalInMs;
	}

	public ConfigurationReader() {
		
	}
	

	public String getPropertyValue(String propertyName) {
		
		// create session factory
				SessionFactory factory = new Configuration()
						.configure("hibernate.cfg.xml")
						.addAnnotatedClass(Property.class)
						.buildSessionFactory();
				
				factory.getProperties().replace("connection.url", this.getConnectionString());
				
				Session session = factory.getCurrentSession();
					
				Property property = null; 
				
				try {			
					// start a transaction
					session.beginTransaction();
								
					Query<Property> theQuery =
							session.
							createQuery("from Property where applicationName=:application and name=:propertyname and isActive=1", 
									Property.class);
					
					theQuery.setParameter("application", this.getApplicationName());
					theQuery.setParameter("propertyname", propertyName);
					
					// execute query and get result list
					if (theQuery.getResultList() == null || theQuery.getResultList().isEmpty()) {
						return null;
					}
					
					property = theQuery.getResultList().get(0);
					
					this.setPropertyName(propertyName);
					this.setPropertyValue(property.getPropertyValue());
					
					
					// commit the transaction
					session.getTransaction().commit();
					
					System.out.println("Done!");
					
				}
				catch(Exception exc) {
					exc.printStackTrace();
				}
				finally {
					factory.close();
				}		
		
		return property.getPropertyValue();	
	}
	
	public String getValue(String propertyName) {
		
		this.setPropertyName(propertyName);
			
        ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
        
        ses.scheduleWithFixedDelay(this, 0, this.getRefreshTimerIntervalInMs(), TimeUnit.MILLISECONDS);       
        
        return propertyValue;
	}

	@Override
	public void run() {
		
		this.propertyValue = getPropertyValue(this.getPropertyName());
		
		System.out.println("************PropertyValue**************   "+ this.getPropertyValue());
		
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getConnectionString() {
		return connectionString;
	}

	public void setConnectionString(String connectionString) {
		this.connectionString = connectionString;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	
	public String getPropertyValue() {
		return propertyValue;
	}

	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}

	public long getRefreshTimerIntervalInMs() {
		return refreshTimerIntervalInMs;
	}

	public void setRefreshTimerIntervalInMs(long refreshTimerIntervalInMs) {
		this.refreshTimerIntervalInMs = refreshTimerIntervalInMs;
	}
}
