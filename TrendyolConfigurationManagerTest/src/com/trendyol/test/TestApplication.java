package com.trendyol.test;

import com.trendyol.serviceapi.ConfigurationReader;

public class TestApplication {

	public static void main(String[] args) {

		// Test the application with the parameters
		
		// applicationName: SERVICE A
		
		// connectionString: "jdbc:mysql://localhost:3306/trendyol?useSSL=false"
		
		//refreshTimerInterval: 2000
		
		String applicationName = "SERVICE A";
		
		String connectionString = "jdbc:mysql://localhost:3306/trendyol?useSSL=false";
		
		// milisecond
		long refreshTimerIntervalInMs = 2000; 
		
		ConfigurationReader configurationReader = new ConfigurationReader(applicationName, connectionString, refreshTimerIntervalInMs);
		
		System.out.println("*** Site Name for SERVICE A is ***    " + configurationReader.getValue("SiteName"));
		
	}

}
