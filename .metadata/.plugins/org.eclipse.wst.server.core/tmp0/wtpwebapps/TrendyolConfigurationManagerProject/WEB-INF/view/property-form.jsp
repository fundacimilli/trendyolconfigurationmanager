<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>

<html>

<head>

	<title>Save Property</title>
	
	<link type="text/css"
			rel="stylesheet"
			href="${pageContext.request.contextPath}/resources/css/style.css">
			
	<link type="text/css"
			rel="stylesheet"
			href="${pageContext.request.contextPath}/resources/css/add-property-style.css">
	
</head>

<body>

	<div id="wrapper">
		<div id="header">
			<h2>TRENDYOL Connection Manager Properties</h2>
		</div>
	</div>
	
	<div id="container">
		<h3>Save Property</h3>
		
		<form:form action="saveProperty" modelAttribute="cmProperty" method="POST">
		
		<!-- need to associate this data with customer id -->
		<form:hidden path="id" />
			
			<table>
				<tbody>
				
					<tr>
						<td><label>Name:</label></td>
						<td><form:input path="name" /></td>
					</tr>
					
					<tr>
						<td><label>Type:</label></td>
						<td><form:input path="propertyType" /></td>
					</tr>
					
					<tr>									
						<td><label>Value:</label></td>
						<td><form:input path="propertyValue" /></td>
					</tr>
					
					<tr>
						<td><label>IsActive:</label></td>
						<td><form:input path="isActive" /></td>
					</tr>	
					
					<tr>
						<td><label>ApplicationName:</label></td>
						<td><form:input path="applicationName" /></td>
					</tr>
					
					<tr>
						<td><label></label></td>
						<td><input type="submit" value="Save" class="save" /></td>
					</tr>
					
				</tbody>
			</table>
		
		
		</form:form>
		
		<div style="clear; both;"></div>
		
		<p>
			<a href="${pageContext.request.contextPath}/properties/list">Back to List</a>
		</p>
		
	</div>
</body>

</html>