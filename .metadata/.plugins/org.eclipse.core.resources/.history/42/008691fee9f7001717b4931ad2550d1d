package com.trendyol.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.trendyol.entity.ConnectionManagerProperty;

@Repository
public class ConnectionManagerDAOImpl implements ConnectionManagerDAO {

	// need to inject the hibernate session factory
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<ConnectionManagerProperty> getConnectionManagerProperties() {

		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		
		// create a query
		Query<ConnectionManagerProperty> theQuery = 
				currentSession.createQuery("from ConnectionManagerProperty", ConnectionManagerProperty.class);
		
		// execute query and get result list
		List<ConnectionManagerProperty> cmProperties = theQuery.getResultList();
		
		// return the results
		return cmProperties;
	}

	@Override
	public void saveProperty(ConnectionManagerProperty theCmProperty) {

		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		
		// save the property
		currentSession.save(theCmProperty);
	}

}
