package com.trendyol.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.trendyol.entity.Property;
import com.trendyol.service.ConnectionManagerService;

@Controller
@RequestMapping("/properties")
public class PropertiesController {
	
	// need to inject connection manager service
	@Autowired
	private ConnectionManagerService cmService;
	
	@GetMapping("/list")
	public String listProperties(Model theModel) {
	
		// get connectionManagerProperties from the service
		List<Property> theCmProperties = cmService.getConnectionManagerProperties();
				
		// add connectionManagerProperties to the model
		theModel.addAttribute("cmProperties", theCmProperties);
		
		
		return "list-properties";
	}
	
	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model theModel) {
		
		// create model attribute to bind form data
		Property cmProperty = new Property();
		
		theModel.addAttribute("cmProperty", cmProperty);
		
		return "property-form";
	}
	
	@PostMapping("/saveProperty")
	public String saveProperty(@ModelAttribute("cmProperty") Property theCmProperty) {
		
		// save the customer using service
		cmService.saveProperty(theCmProperty);
		
		return "redirect:/properties/list";
	}

}
