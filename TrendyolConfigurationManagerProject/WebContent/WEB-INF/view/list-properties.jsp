<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>

<html>

<head>
	<title> List Connection Manager Properties </title>
	
	<!-- reference style sheet -->
	
	<link type="text/css"
			rel="stylesheet"
			href="${pageContext.request.contextPath}/resources/css/style.css" />
	
</head>

<body>

	<div id="wrapper">
		<div id="header">
			<h2>TRENDYOL Connection Manager Properties</h2>
		</div>
	</div>
	
	<div id="container">
	
		<div id="content">
		
		<!-- put new button: Add ConnectionManagerProperty -->
		
		<input type="button" value="Add Property"
				onclick="window.location.href='showFormForAdd'; return false;"
				class="add-button"
		/>
		
		<!--  add a search box -->
			<form:form action="search" method="POST">
				Search property: <input type="text" name="theSearchName" />
				
				<input type="submit" value="Search" class="add-button" />
			</form:form>
		
		<!--  add out html table here -->
		
		<table>
			<tr>
				<th>Name</th>
				<th>Type</th>
				<th>Value</th>
				<th>IsActive</th>
				<th>ApplicationName</th>
				<th>Action</th>
			</tr>
			
			<!-- loop over and print our connectionManagerProperties -->
			<c:forEach var="tempCmProperty" items="${cmProperties}">
			
				<!-- construct an "update" link for properties -->
				<c:url var="updateLink" value="/properties/showFormForUpdate">
					<c:param name="propertyId" value="${tempCmProperty.id}" />
				</c:url>
				
				<!-- construct an "delete" link for properties -->
				<c:url var="deleteLink" value="/properties/delete">
					<c:param name="propertyId" value="${tempCmProperty.id}" />
				</c:url>
				
				<tr>
					<td>${tempCmProperty.name}</td>
					<td>${tempCmProperty.propertyType}</td>
					<td>${tempCmProperty.propertyValue}</td>
					<td>${tempCmProperty.isActive}</td>
					<td>${tempCmProperty.applicationName}</td>
					
					<td>
						<!-- display the update and link -->
						<a href="${updateLink}">Update</a>
						|
						<!-- add js to prompt the user -->
						<a href="${deleteLink}" 
							onclick="if (!(confirm('Are your sure you want to delete this property?'))) return false">Delete</a>
					</td>
					
				</tr>
				
			</c:forEach>
				
		</table>
		
		</div>
	
	</div>

</body>

</html>