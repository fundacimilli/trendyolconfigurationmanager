package com.trendyol.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trendyol.dao.ConnectionManagerDAO;
import com.trendyol.entity.Property;

@Service
public class ConnectionManagerServiceImpl implements ConnectionManagerService {

	// need to inject connection manager dao
	@Autowired
	private ConnectionManagerDAO connectionManagerDAO;
	
	@Override
	@Transactional
	public List<Property> getConnectionManagerProperties() {

		return connectionManagerDAO.getConnectionManagerProperties();
	}

	@Override
	@Transactional
	public void saveProperty(Property theCmProperty) {

		connectionManagerDAO.saveProperty(theCmProperty);
	}

	@Override
	@Transactional
	public Property getProperty(int theId) {

		return connectionManagerDAO.getProperty(theId);
	}

	@Override
	@Transactional
	public void deleteProperty(int theId) {
		
		connectionManagerDAO.deleteProperty(theId);
		
	}

	@Override
	@Transactional
	public List<Property> searchProperties(String theSearchName) {

		
		return connectionManagerDAO.searchProperties(theSearchName);
	}

}
