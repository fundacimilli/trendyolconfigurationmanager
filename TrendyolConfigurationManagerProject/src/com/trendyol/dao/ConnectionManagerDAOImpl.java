package com.trendyol.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.trendyol.entity.Property;

@Repository
public class ConnectionManagerDAOImpl implements ConnectionManagerDAO {

	// need to inject the hibernate session factory
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Property> getConnectionManagerProperties() {

		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		
		// create a query
		Query<Property> theQuery = 
				currentSession.createQuery("from Property order by applicationName", Property.class);
		
		// execute query and get result list
		List<Property> cmProperties = theQuery.getResultList();
		
		// return the results
		return cmProperties;
	}

	@Override
	public void saveProperty(Property theCmProperty) {

		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		
		// save or update the property
		currentSession.saveOrUpdate(theCmProperty);
	}

	@Override
	public Property getProperty(int theId) {

		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		
		Property theProperty = currentSession.get(Property.class, theId);
		
		// return the results
		return theProperty;
		
	}

	@Override
	public void deleteProperty(int theId) {
		
		Session currentSession = sessionFactory.getCurrentSession();
		
		Query<?> theQuery =
				currentSession.createQuery("delete from Property where id=:propertyId");
		
		theQuery.setParameter("propertyId", theId);
		
		theQuery.executeUpdate();
		
	}

	@Override
	public List<Property> searchProperties(String theSearchName) {

		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		
		Query<Property> theQuery = null;
		
		//
		// only search by name if theSearchName is not empty
		//
		if (theSearchName != null && theSearchName.trim().length() > 0) {

			// search for name ... case insensitive
			theQuery =currentSession.createQuery("from Property where lower(name) like :theName", Property.class);
			theQuery.setParameter("theName", "%" + theSearchName.toLowerCase() + "%");

		}
		else {
			// theSearchName is empty ... so just get all properties
			theQuery =currentSession.createQuery("from Property", Property.class);			
		}
		
		// execute query and get result list
		List<Property> properties = theQuery.getResultList();
				
		// return the results		
		return properties;
		
	}

}
