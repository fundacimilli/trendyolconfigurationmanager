package com.trendyol.dao;

import java.util.List;

import com.trendyol.entity.Property;

public interface ConnectionManagerDAO {
	
	public List<Property> getConnectionManagerProperties();

	public void saveProperty(Property theCmProperty);

	public Property getProperty(int theId);

	public void deleteProperty(int theId);

	public List<Property> searchProperties(String theSearchName);

}
