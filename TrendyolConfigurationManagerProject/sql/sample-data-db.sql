
USE trendyol;
    
INSERT INTO `connection_manager_properties` VALUES 
	(1,'SiteName', 'String', 'trendyol.com', 1, 'Service A'),
	(2,'IsBasketEnabled', 'Boolean', 1, 1, 'Service A'),
	(3,'MaxItemCount', 'Int', 50, 0, 'Service A'),
    (4,'SiteName', 'String', 'funda.com', 1, 'Service B'),
	(5,'IsBasketEnabled', 'Boolean', 0, 0, 'Service B'),
	(6,'MaxItemCount', 'Int', 40, 1, 'Service B'),
    (7,'SiteName', 'String', 'kubra.com', 1, 'Service C'),
	(8,'IsBasketEnabled', 'Boolean', 1, 1, 'Service C'),
	(9,'MaxItemCount', 'Int', 30, 0, 'Service C')
    
    
    