DROP SCHEMA IF EXISTS `trendyol`;

CREATE SCHEMA `trendyol`;

use `trendyol`;


DROP TABLE IF EXISTS `connection_manager_properties`;

CREATE TABLE `connection_manager_properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `property_type` varchar(45) DEFAULT NULL,
  `property_value` varchar(45) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `application_name` varchar(45) DEFAULT NULL,
  
  PRIMARY KEY (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;


