# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This project is for Trendyol challange
  Project consists of three sub projects
  
  TrendyolConfigurationManager: The Spring MVC and Hibernate project for configuring all properties in DB via a UI
  
  TrendyolConfigurationManagerServiceApi: The Hibernate project for custom library to add requirements of challange
  										  The Api Constractor meets the user preferences and has a signiture to give property value based on property name.
  
  TrendyolConfigurationManagerTest: This is the test project including TrendyolConfigurationManagerApi as a JAR and test data
  
* Version 1.0

### How do I get set up? ###

 For Windows Configuration
 ====================================
 
* Set up TOMCAT server 9.0

* Set up MySql server and MySqlClient

* DB connection: 
  localhost: 3306
  db: trendyol
  user: trendyol
  pass: trendyol

* SQL test data scripts are under the project TrendyolConfigurationManager sql folder

* You can directly connect to the Admin GUI by the link below from browser
  The war file provided should be located under the TOMCAT webapps directory
  
  http://localhost:8080/TrendyolConfigurationManagerProject/properties/list
  
  You can perform all CRUD operations via the GUI interface

### Contribution guidelines ###

* Writing tests

	There are two test applications; 
	
	DB connection application located under project TrendyolConfigurationManager
	TrendyolConfigurationManagerTest is for testing main functionalities

### Frameworks and Technologies ###

* MySQL and MySQL connector as database and client

* Hibernate 5.2.12 as ORM

* HQuery for DB queries

* Hibernate SessionFactory and Transactions for DB connections

* Spring framework 5.0.2

* Spring MVC for web app

* Spring DI and IOC for DAO and Service injection and construction

* JSP for views




